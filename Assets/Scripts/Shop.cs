﻿
using UnityEngine;

public class Shop : MonoBehaviour {

	public TowerBlueprint tower1;
	public TowerBlueprint tower2;
	public TowerBlueprint tower3;
	BuildManager buildManager;

	void Start() {
		buildManager = BuildManager.instance;
	}

	public void SelectTower1 () {
		
		buildManager.SelectTowerToBuild (tower1);

	}

	public void SelectTower2 () {
		
		buildManager.SelectTowerToBuild (tower2);

	}

	public void SelectTower3 () {

		buildManager.SelectTowerToBuild (tower3);

	}
}
