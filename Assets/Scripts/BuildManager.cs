﻿
using UnityEngine;

public class BuildManager : MonoBehaviour {

	public static BuildManager instance;


	private TowerBlueprint towerToBuild;

	//Lesson 18 - Upgrade
	private Node selectedNode;
	public NodeUI nodeUI;

	public GameObject buildEffect;


//	TowerDictionary towerDictionary;

	//called before Start()
	void Awake() {
		if (instance != null) {
			Debug.Log ("More than one BuildManager in scene!");
			return;
		}	
		instance = this;
	}



	void Start() {
//		towerDictionary = TowerDictionary.instance;
		EventManager.onEmptyNodeClick += this.SelectNode;
		EventManager.onTowerClick += this.SelectTower;
	}

	void OnDestroy() {
		//		towerDictionary = TowerDictionary.instance;
		EventManager.onEmptyNodeClick -= this.SelectNode;
		EventManager.onTowerClick -= this.SelectTower;
	}

	//public bool CanBuild{get {return towerToBuild != null;}}
	public bool HasMoney{get {return PlayerStats.GetMoney() >= towerToBuild.cost;}}

	public void SelectTowerToBuild (TowerBlueprint tower) {
		towerToBuild = tower;

		DeselectNode ();
	}

	//Lesson 18 - Upgrade
	public void SelectNode (Node node) {

		if (node == null) {
			Debug.Log ("SelectNode - NODE IS NULL - Quiting function");
			return;
		}
		nodeUI.Hide ();

		if (selectedNode == node) {
			//Debug.Log ("BuildMAnager - Selected Node same as previous node - Hide Menu");
			selectedNode = null;
			node.UnselectNode ();
			return;
		}

//		Debug.Log ("BuildMAnager - Selected Node is new, now determine which menu to show");

		selectedNode = node;
		node.SelectNode ();

		//Decide which menu to display
		if (node.towerGO == null) { //Display Build Menu			
			//Debug.Log ("BuildMAnager - Display BuyTowerMenu");
			nodeUI.SetMenuPosition (node);
			nodeUI.ShowBuyTowerMenu ();
		} else { //Display Upgrade Menu
			//Debug.Log ("BuildMAnager - Display ManageTowerMenu");
			nodeUI.SetMenuPosition (node);
			nodeUI.ShowManageTowerMenu (node);
		}





	}

	public void SelectTower(TowerMaster towerMaster) {
//		Debug.Log("Tower selected");
		SelectNode(towerMaster.GetNode());

	}
	public void DeselectNode() {
		//Debug.Log ("BuildMAnager - DeselectNode");
		selectedNode = null;
		nodeUI.Hide ();
	}


//	public void BuildTowerOn (Node node) {
//
//		if (PlayerStats.Money < towerToBuild.cost) {
//			Debug.Log ("Not enough money to build");
//			return;
//		}
//
//		PlayerStats.Money -= towerToBuild.cost;
//		Debug.Log ("Tower built, money left " + PlayerStats.Money);
//		GameObject tower = (GameObject) Instantiate (towerToBuild.prefab, node.GetBuildPosition(), Quaternion.identity);
//		node.tower = tower;
//
//		GameObject effect = (GameObject) Instantiate (buildEffect, node.GetBuildPosition (), Quaternion.identity);
//		Destroy (effect, 5f);
//	}

	public void BuildTower(Node targetNode, GameObject prefabName, int cost ) {
		

		//Instanstiate the Tower
		GameObject towerGO = (GameObject) Instantiate (prefabName, targetNode.GetBuildPosition(), Quaternion.Euler(new Vector3(0, 225, 0)));

		//Assign the tower in the Node
		targetNode.towerGO = towerGO;

		//Assign the Node in the tower
		towerGO.GetComponent<TowerMaster> ().SetNode (targetNode);

		//Play build effects
		GameObject effect = (GameObject) Instantiate (buildEffect, targetNode.GetBuildPosition (), Quaternion.identity);
		Destroy (effect, 3f);

		//Raise TowerBuilt event
		EventManager.TowerBuilt (cost);

		DeselectNode ();


	}

	public void UpgradeTower (TowerMaster tm, int cost) {
//		Debug.Log("Upgrade Tower called");
		tm.Upgrade();

		//Play build effects
		GameObject effect = (GameObject) Instantiate (buildEffect, tm.GetNode().GetBuildPosition (), Quaternion.identity);
		Destroy (effect, 3f);

		EventManager.TowerBuilt (cost);
		DeselectNode ();

	}
}
