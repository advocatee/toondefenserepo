﻿
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {

	public Text uiMoneyText;
	public int startMoney = 400;
	private static int currentMoney;

	public Text uiLivesText;
	public int startLives = 20;
	private static int currentLives;

	public static int threshold3s = 20;
	public static int threshold2s = 15;
	public static int threshold1s = 0;

	public static int RoundsSurvived;



	void Start() {

		//setup Event Listeners
		EventManager.onEnemyDestroy += this.AddMoney;
		EventManager.onTowerBuilt += this.DecreaseMoney;
		EventManager.onEnemyReachedEnd += this.DecreaseLives;

		//setup money
		currentMoney = startMoney;
	
		//setup for lives
		currentLives = startLives;

		RoundsSurvived = 0;


	}

	void Update() {
		uiMoneyText.text = currentMoney.ToString();
		uiLivesText.text = currentLives.ToString();
	}

	private void AddMoney(int value) {
		//Debug.Log("PlayerStats: AddMoney called with value " + value)	;
		currentMoney += value;
	}

	private void DecreaseMoney(int value) {
		//Debug.Log("PlayerStats: DecreaseMoney called with value " + value)	;
		currentMoney -= value;
	}

	public static int GetRating() {
		if (currentLives >= threshold3s) {
			return 3;
		} else if (currentLives >= threshold2s) {
			return 2;
		} else if(currentLives >= threshold1s) {
			return 1;
		} else {
			return 0;
		}
	}
	public static int GetMoney() {
		return currentMoney;
	}

	private void AddLives(int value) {
		//Debug.Log("PlayerStats: AddLives called with value " + value)	;
		currentLives += value;
	}

	private void DecreaseLives(int value) {
		//Debug.Log("PlayerStats: DecreaseLives called with value " + value)	;
		currentLives -= value;
	}

	public static int GetLives() {
		return currentLives;
	}

	void OnDestroy() {
		//Debug.Log ("PlayerStats On Destroy called");
		//setup Event Listeners
		EventManager.onEnemyDestroy -= this.AddMoney;
		EventManager.onTowerBuilt -= this.DecreaseMoney;
		EventManager.onEnemyReachedEnd -= this.DecreaseLives;
	}
}
