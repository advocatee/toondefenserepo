﻿
using UnityEngine;
using UnityEngine.UI;
public class NodeUI : MonoBehaviour {



	public GameObject[] towerPrefabs;	
	private Node targetNode;

	//UI references
	public GameObject buttonPrefab;
	public GameObject buyTowerUI;
	public GameObject manageTowerUI;

	//Manage Tower UI stuff, need to store reference as Manage UI changes everytime a tower is clicked
	private GameObject manageTowerPanel;
	private GameObject upgradeButtonGO;
	private Button upgradeButton;


	BuildManager buildManager;

	void Awake() {
		manageTowerUI = gameObject.transform.Find ("ManageTowerUI").gameObject;
		manageTowerPanel = manageTowerUI.transform.Find ("ManageTowerPanel").gameObject;
		upgradeButtonGO = manageTowerPanel.transform.Find ("UpgradeButton").gameObject;
		upgradeButton = upgradeButtonGO.GetComponent<Button> ();
	}
	void Start() {
		
		buildManager = BuildManager.instance;


		//Create the buyTower menu
		GameObject buyTowerUI = gameObject.transform.Find ("BuyTowerUI").gameObject;
		GameObject buyTowerPanel = buyTowerUI.transform.Find ("BuyTowerPanel").gameObject;

		foreach (GameObject tower in towerPrefabs) {
			
			TowerMaster tm = tower.GetComponent<TowerMaster> ();
			TowerBlueprint tb = tm.GetStartTowerBlueprint ();

			//Instantiate the button
			GameObject buyButton = (GameObject)Instantiate (buttonPrefab, buyTowerPanel.transform.position, buyTowerPanel.transform.rotation, buyTowerPanel.transform);

			//Set button image
//			button.GetComponent<Image> ().sprite = towerRoadmap.towerBlueprints [0].buttonThumbnail;
			buyButton.GetComponent<Image> ().sprite = tb.buttonThumbnail;

			//Set button text
			GameObject textObject = buyButton.transform.Find("Price").gameObject;
			Text cost = textObject.GetComponent<Text> ();
			cost.text = tb.cost.ToString ();
//
//			//This seems to work
//			//Set the upgrade prefab in the AddListener call, if no upgrade, set as null
//			GameObject upgradePrefab = null;
//			if (towerRoadmap.towerBlueprints.Length > 1) {
//				 upgradePrefab = towerRoadmap.towerBlueprints [1].prefab;
//			}
//				
//			//Finally AddListener
//			button.GetComponent<Button>().onClick.AddListener(delegate{buildManager.BuildTowerTest(targetNode, towerRoadmap.towerBlueprints [0].prefab, towerRoadmap.towerBlueprints [0].cost, upgradePrefab);});
			buyButton.GetComponent<Button>().onClick.AddListener(delegate{buildManager.BuildTower(targetNode, tower, tb.cost);});


		}


	}

	public void SetMenuPosition(Node _target) {
		targetNode = _target;
		transform.position = targetNode.GetBuildPosition ();
	}

	public void ShowBuyTowerMenu() {
		
		buyTowerUI.SetActive (true);


	}

	public void ShowManageTowerMenu(Node node) {

		//Reset the upgrade button - remove previous listener, and disable it
		upgradeButton.GetComponent<Button> ().onClick.RemoveAllListeners ();
		upgradeButtonGO.SetActive (false);


		TowerMaster tm = node.towerGO.GetComponent<TowerMaster>();

		if (tm.CanBeUpgraded()){
			
			TowerBlueprint tb = tm.GetUpgradeTowerBlueprint ();
			
			//GameObject button = (GameObject)Instantiate (buttonPrefab, manageTowerPanel.transform.position, Quaternion.identity, manageTowerPanel.transform);
			upgradeButton.GetComponent<Image> ().sprite = tb.buttonThumbnail;
			
			GameObject textObject = upgradeButton.transform.Find("Price").gameObject;
			Text cost = textObject.GetComponent<Text> ();
			cost.text = tb.cost.ToString ();
			
			
			upgradeButton.GetComponent<Button>().onClick.AddListener(delegate{buildManager.UpgradeTower(tm, tb.cost);});

			upgradeButton.enabled = true;
			upgradeButtonGO.SetActive (true);
			
		}
		manageTowerUI.SetActive (true);

	}

	public void Hide() {
		buyTowerUI.SetActive (false);
		manageTowerUI.SetActive (false);

	
	}
}
