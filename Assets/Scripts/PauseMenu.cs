﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {


	public GameObject ui;

	public SceneFader sceneFader;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)) {
			TogglePause ();
		}
	}

	public void TogglePause() {
		ui.SetActive (!ui.activeSelf); //flip the state

		if(ui.activeSelf) {
			Time.timeScale = 0f;
		} else {
			Time.timeScale = 1f;
		}
	}

	public void  Retry() {
		TogglePause ();
		//SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		sceneFader.FadeTo (SceneManager.GetActiveScene ().name);
	}

	public void Menu() {
		//Debug.Log ("Go to menu");
		TogglePause ();
		sceneFader.FadeTo ("MainMenu");
	}

	public void ToggleSpeed() {
		
		if(Time.timeScale == 1f)
			Time.timeScale = 2f;
		else
			Time.timeScale = 1f;

	}
}
