﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	public Text roundsSurvivedText;

	void OnEnable () {
		roundsSurvivedText.text = PlayerStats.RoundsSurvived.ToString ();
	}


	public void Retry() {
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
	}


	public void Menu() {
		Debug.Log ("go to menu");
	}
}
