﻿using UnityEngine;

public class Waypoints : MonoBehaviour {
	//static so the field can be access from anywhere
	public static Transform[] points;

	void Awake() { 
		//Find all the objects (waypoints) of the parent that is passed in
		points = new Transform[transform.childCount];
		for (int i = 0; i < points.Length; i++) {
			points [i] = transform.GetChild (i);
		}
	}


}
