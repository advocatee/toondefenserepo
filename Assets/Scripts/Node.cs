﻿
using UnityEngine;


//use EventSystems so that onMOuseEnter onMouseDown don't trigger if something is 
//between the mouse and the node (e.g some other UI element)
using UnityEngine.EventSystems; 


public class Node : MonoBehaviour {

	public Color hoverColor;
	public Color selectedColor;

	public Vector3 towerPositionOffset;

	private Renderer rend;
	private Color startColor;

	[Header("Optional")]
	public GameObject towerGO; //tower built on top of node (if any)

	//BuildManager buildManager;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer> ();
		startColor = rend.material.color;

		//buildManager = BuildManager.instance;
	}
//	void OnMouseEnter () {
//		
//		if (EventSystem.current.IsPointerOverGameObject ()) {
//			
//			return;
//		}
//
//
//			rend.material.color = hoverColor;	
//
//
//	}

//	void OnMouseExit() {
//		rend.material.color = startColor;
//	}

	void OnMouseUp() {

		if (EventSystem.current.IsPointerOverGameObject ()) {
			Debug.Log ("Node ignoring click as mouse is over a UI object");
			return;
		}

		if (towerGO != null) {
			return;
		}




		//Set Selected Node only
		//buildManager.SelectNode (this);

		EventManager.EmptyNodeClick (this);

		//buildManager.BuildTowerOn (this);
	}

	public Vector3 GetBuildPosition () {
		return transform.position + towerPositionOffset;
	}

	public void SelectNode() {
		rend.material.color = selectedColor;
	}
	public void UnselectNode() {
		rend.material.color = startColor;
	}

}
