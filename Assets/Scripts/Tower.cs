﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tower : MonoBehaviour {

	public Transform targetTransform; //if u wanna see what the target is in-game, leave as public
	private Enemy targetScript;


	[Header("General")]
	public float range = 15f;

	[Header("Use Bullets (default)")]
	public GameObject bulletPrefab;
	public float fireRate = 1f; //fire once a second
	private float fireCountdown = 0f;

	[Header("Use Laser")]
	public bool useLaser = false;
	public LineRenderer lineRenderer;
	public ParticleSystem impactEffect;

	[Header("Unity Setup Fields")]
	public string enemyTag = "Enemy";
	public Transform partToRotate;
	public float rotateSpeed = 10f;
	public Transform firePoint;
	public Animator anim;

	// Use this for initialization
	void Start () {
		//Call update target every 0.5 secs
		InvokeRepeating ("UpdateTarget", 0f, 0.5f);

	}

	void UpdateTarget() {
		GameObject[] enemies = GameObject.FindGameObjectsWithTag (enemyTag);

		float shortestDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;

		foreach (GameObject enemy in enemies) {
			float distanceToEnemy = Vector3.Distance (transform.position, enemy.transform.position);
			if (distanceToEnemy < shortestDistance && enemy.GetComponent<Enemy>().isAlive) { //check distance and that enemy is still alive!
				shortestDistance = distanceToEnemy;
				nearestEnemy = enemy;
			}
		}

		//by now we should have the closest enemy stored in nearestEnemy

		if (nearestEnemy != null && shortestDistance <= range) {
			targetTransform = nearestEnemy.transform;
			targetScript = nearestEnemy.GetComponent<Enemy> ();
		} else {
			targetTransform = null;
			targetScript = null;
		}
	}

	// Update is called once per frame
	void Update () {
		if (targetScript != null) {
			if (targetScript.isAlive == false) { //enemy just died so target something else
				UpdateTarget ();
				return;
			}
		}

		if (targetTransform == null) {
			if (useLaser) {
				if (lineRenderer.enabled) {
					lineRenderer.enabled = false;
					impactEffect.Stop ();
				}
			}
			return;
		}

		LockOnTarget ();

		if (useLaser) {
			
			Laser ();

		} else {
			
			if (fireCountdown <= 0f) {
				Shoot ();
				fireCountdown = 1f / fireRate;
			}
			fireCountdown -= Time.deltaTime;	
		}


	}

	void LockOnTarget() {

		if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Armature|Shoot")) //only play lockon if shoot animation is not playing / has ended
			anim.Play ("Armature|Lockon");

		//Get a vector3 that will point in the direction of our enemy
		//always take destination - origin
		// should be be transform of the main gameobject, of the transform of the parttorotate?
		Vector3 dir = targetTransform.position - transform.position; 
		Quaternion lookRotation = Quaternion.LookRotation (dir);
		Vector3 rotation = Quaternion.Lerp(partToRotate.rotation,lookRotation,Time.deltaTime * rotateSpeed).eulerAngles;
		//only rotate on the Y axis
		partToRotate.rotation = Quaternion.Euler (0f, rotation.y, 0f);

	}

	void Laser() {
		if (!lineRenderer.enabled) {
			lineRenderer.enabled = true;
			impactEffect.Play (); //play particle system
		}
		//set start and end points of lineRenderer, index 0 and 1
		lineRenderer.SetPosition (0, firePoint.position);
		lineRenderer.SetPosition (1, targetTransform.position);


		//set rotation of laser impactEffect
		Vector3 dir = firePoint.position - targetTransform.position;
		//impactEffect.transform.lookAt
		impactEffect.transform.rotation = Quaternion.LookRotation (dir);

		//set position laser impactEffect
		impactEffect.transform.position = targetTransform.position + dir.normalized * 0.5f;

	}
	void Shoot() {

		anim.Play ("Armature|Shoot");

		//instantiate bullet, need to cast instantiated object when getting a reference to it
		GameObject bulletGO = (GameObject) Instantiate (bulletPrefab, firePoint.position, firePoint.rotation);
		Bullet bullet = bulletGO.GetComponent<Bullet> ();
		if (bullet != null) {
			bullet.Seek (targetTransform);
		}
	}
	void OnDrawGizmosSelected () {
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (transform.position, range);
	}
}
