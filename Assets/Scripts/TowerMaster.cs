﻿
using UnityEngine;
using UnityEngine.EventSystems; 

public class TowerMaster : MonoBehaviour {

	public int startTowerIndex = 0;
	private int nextTowerUpgradeIndex;
	public TowerBlueprint[] towerBlueprints;
	private GameObject currentTowerGO;
	private Node node;

	public void testAccess() {
		Debug.Log ("TowerMaster testAccess is accessible");
	}
	void Awake() {
		
		nextTowerUpgradeIndex = startTowerIndex;
		nextTowerUpgradeIndex++;

	}


	// Use this for initialization
	void Start () {
//		Debug.Log ("TowerMaster START(), instantiate tower of index " + startTowerIndex + ". Next Upgrade is index " + nextTowerUpgradeIndex);

		currentTowerGO = (GameObject) Instantiate (towerBlueprints[startTowerIndex].prefab, transform.position, transform.rotation, transform);
	}

	public void SetNode(Node _node) {
		node = _node;
	}

	public Node GetNode() {
		return node;
	}


	public TowerBlueprint GetStartTowerBlueprint() {
		return towerBlueprints [startTowerIndex];
	}

	public bool CanBeUpgraded() {
		if (nextTowerUpgradeIndex + 1 > towerBlueprints.Length) {
			return false;
		} else {
			return true;
		}
	}

	public TowerBlueprint GetUpgradeTowerBlueprint() {
		return towerBlueprints [nextTowerUpgradeIndex];
	}

//	public Sprite GetButtonIconForUpgrade () {
//		
//	}


	public void Upgrade() {

		Destroy (currentTowerGO);

//		Debug.Log ("Upgrading tower to TowerBlueprint index" + nextTowerUpgradeIndex + " which is prefab: " + towerBlueprints[nextTowerUpgradeIndex].prefab.name);
		currentTowerGO = (GameObject) Instantiate (towerBlueprints[nextTowerUpgradeIndex].prefab, transform.position, transform.rotation, transform);

		nextTowerUpgradeIndex++;
//		Debug.Log("Tower Upgraded, next blueprint index set as " + nextTowerUpgradeIndex);


	}

	void OnMouseDown() {
//		Debug.Log ("TOWERMASTER CLICKED");

		if (EventSystem.current.IsPointerOverGameObject ())
			return;

		EventManager.TowerClick (this);
	}
}

