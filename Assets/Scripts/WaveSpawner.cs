﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class WaveDetail {
	public Transform enemyPrefab;
	public int spawnCount;
	public float timeBetweenSpawns; //Seconds between each enemy spawn
	public float waveDuration; //How long does this wave last before next wave
}

public class WaveSpawner : MonoBehaviour {

	public float initialCountdown = 3f;
	public float waveCountDown = 0f;
	public Transform spawnPoint; // reference to spawnpoint
	public Text waveCountDownText; //reference to text object
	public int waveStartIndex = 0;
	public WaveDetail[] publicWaveDetails;

//	private WaveDetail[] privateWaveDetails;

//	private float waveCountDown; 

	private int waveCurrentIndex = -1;
	private int waveFinalIndex;



//	private GameObject[] enemies;

	void Start() {



		waveFinalIndex = publicWaveDetails.Length  - 1;
		waveCurrentIndex = waveStartIndex;
//		privateWaveDetails = publicWaveDetails;

	}

	void OnDestroy() {
		
	}



	void Update() {



		if (initialCountdown > 0f) {
//			Debug.Log ("initial countdown " + initialCountdown);
			initialCountdown -= Time.deltaTime; 
			initialCountdown = Mathf.Clamp (initialCountdown, 0f, Mathf.Infinity);
			waveCountDownText.text = string.Format ("{0:00.00}", initialCountdown);
			return;
		}

//		Debug.Log ("waveCurrentIndex is " + waveCurrentIndex + ", waveFinalIndex is " + waveFinalIndex);
		if (waveCountDown <= 0f && waveCurrentIndex <= waveFinalIndex) {
			
			StartCoroutine (SpawnWave (waveCurrentIndex));//this is how u call a coroutine (system.collections)

			//set countdown to current wave duration
			waveCountDown = publicWaveDetails [waveCurrentIndex].waveDuration;

			//increment waveCurrentIndex
			waveCurrentIndex++;



		}	

		if (waveCurrentIndex <= waveFinalIndex) {
			
			waveCountDown -= Time.deltaTime;
			waveCountDown = Mathf.Clamp (waveCountDown, 0f, Mathf.Infinity);
			waveCountDownText.text = string.Format ("{0:00.00}", waveCountDown);
			
		} else {
//			Debug.Log ("waveCurrentIndex is " + waveCurrentIndex + ", waveFinalIndex is " + waveFinalIndex);
			waveCountDownText.text = "Final Wave";
		}
	


	}

	//IEnumerator (system.collections) to be called as a coroutine
	IEnumerator SpawnWave(int index) {
		


		//Debug.Log ("Wave " + waveIndex + " Incoming");

		PlayerStats.RoundsSurvived++;



		for (int i = 0; i < publicWaveDetails [index].spawnCount; i++) {

			Instantiate (publicWaveDetails [index].enemyPrefab, spawnPoint.position, spawnPoint.rotation);
			EventManager.EnemySpawned ();
			yield return new WaitForSeconds (publicWaveDetails [index].timeBetweenSpawns); //wait for moment before next iteration
		}

		if (index == waveFinalIndex) { 
			//by the time we reached here, we would have spawned all enemies in the final wave
			EventManager.AllEnemiesSpawned ();
		}

	}


}
