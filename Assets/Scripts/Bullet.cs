﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	private Transform target;
	public float speed = 70f;
	public float explosionRadius = 10f;
	public int damage = 50;
	public GameObject impactEffect;


	public void Seek(Transform _target) {
		target = _target;
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		//If target is null or destroye before impact, do what?
		if (target == null) {
			Destroy (gameObject);
			return;
		}	

		//Establish direction of movement every frame based on current position & target position
		//gives a homing effect if bullet speed is lowered
		Vector3 dir = target.position - transform.position;
		float distanceThisFrame = speed * Time.deltaTime;

		//if directons magnitude (distance) between bullet and target is less than 
		// distance calculated to travel this frame (based on speed param)
		//it means bullet will overshoot; i.e will hit target in next frame
		if (dir.magnitude <= distanceThisFrame) {
			HitTarget ();
			return;
		}

		//FINALLY, move the bullet
		transform.Translate (dir.normalized * distanceThisFrame, Space.World);
		//this keeps it looking / rotated at the target
		transform.LookAt (target);
	}

	void HitTarget() {
		//Debug.Log ("we hit something");
		GameObject effectInstance = (GameObject) Instantiate (impactEffect, transform.position, transform.rotation);
		Destroy (effectInstance, 5f);

		if (explosionRadius > 0f) {
			
			Explode ();
		} else {
			Damage (target.transform);
		}

		Destroy (gameObject);
	}

	void Damage (Transform enemy) {
		Enemy e = enemy.GetComponent<Enemy> (); //Gets the Enemy Script component
		if (e != null) {
			e.TakeDamage (damage);
		}

	}

	void Explode() {
		
		Collider[] colliders = Physics.OverlapSphere (transform.position, explosionRadius);
		foreach (Collider collider in colliders) {
			
			if (collider.tag == "Enemy") {
				//Debug.Log ("Collided name is " + collider.name + ", Collided tag is " + collider.tag.ToString ());	
				Damage (collider.transform);
			}
		}
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere (transform.position, explosionRadius);

	}
}
