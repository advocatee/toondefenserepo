﻿
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

	[Header("Attributes")]
	public float speed = 5f;
	public float startHealth = 100;
	public int moneyValue = 50;
	public bool targetable = true;

	public Animator anim;

	private Transform target;
	private int waypointIndex = -1;

	public bool isAlive = true;

	private float currentHealth;

	[Header("Unity Stuff")]
	public Image healthBar;
	public Text healthText;
//	public Camera camera;

	void Awake() {
		
	}
	void Start () {

		currentHealth = startHealth;
		//target as first waypoint, note waypoints.points is a static array :)
		anim.Play ("Walking");
		//Debug.Log ("walking");

		GetNextWaypoint ();
	}
		

	public void TakeDamage (float amount) {
		//Debug.Log ("taking " + amount.ToString () + " damage");
		currentHealth -= amount;
		healthBar.fillAmount = currentHealth / startHealth;

		if (currentHealth <= 0) {
			if(isAlive) {
				Die ();				
			}
		}
	}

	private void Die() {
		isAlive = false;
		transform.tag = "Untagged";

		anim.Play ("Die");

		EventManager.EnemyDestroyed (moneyValue);

		Destroy (gameObject, 3f);

		this.enabled = false;


	}
	void Update () {


		//establish the direction of the movement
		Vector3 dir = target.position - transform.position;

		//dir.y = 0; //set Y as zero so monsters don't adjust in Y position

		//normalize  returns magnitude of 1, move in world space (must be specify this?)
		transform.Translate (dir.normalized * speed * Time.deltaTime, Space.World); 


		//Attempt to rotate based on direction
		transform.LookAt (target);

		//Attempt to rotate the healthbar
		healthBar.transform.rotation = Camera.main.transform.rotation;
		healthText.transform.rotation = Camera.main.transform.rotation;


		//check if our distance from current pos to waypoint is less than a certain threshold
		if (Vector3.Distance(transform.position, target.position) <= 0.2f) {
			//get the next waypoint
			GetNextWaypoint();
		}



		//kev - temp
		healthText.text = currentHealth.ToString();
	}

	void GetNextWaypoint() {
		waypointIndex++;

		if (waypointIndex > Waypoints.points.Length - 1) {
			//Debug.Log ("Next waypoint index set as " + waypointIndex.ToString () + " - Ended as no such waypoint");
			EndPath();
			return;
		} 
		//Debug.Log ("Next waypoint index set as " + waypointIndex.ToString ());

		//Debug.Log ("Gameobject " + gameObject.name.ToString() + ": heading towards WAYPOINTINDEX " + waypointIndex.ToString() + ", TOTAL WAYPOINTS is " + Waypoints.points.Length.ToString());
		target = Waypoints.points [waypointIndex];
	}


	void EndPath() {
		
		EventManager.EnemyReachedEnd (1);
		Destroy (gameObject); 

		return; // Destroy may take a while, so return and don't execute the rest of code
	}
}
