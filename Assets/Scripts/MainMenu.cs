﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public string levelToLoad = "MainScene";
	public SceneFader sceneFader;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Play() {
		Debug.Log ("Play");
		//SceneManager.LoadScene (levelToLoad);
		sceneFader.FadeTo (levelToLoad);
	}

	public void UITest() {
		sceneFader.FadeTo ("UITesting");
	}

	public void Quit() {
		Debug.Log ("Quit");
		Application.Quit ();
	}
}
