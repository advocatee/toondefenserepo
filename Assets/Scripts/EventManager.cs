﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {


	public delegate void EnemyEventHandler (int value);
	public static event EnemyEventHandler onEnemyDestroy;
	public static event EnemyEventHandler onEnemyReachedEnd;

	public delegate void TowerEventHandler (int value);
	public static event TowerEventHandler onTowerBuilt;

	public delegate void NodeClickHandler (Node node);
	public static event NodeClickHandler onEmptyNodeClick;

	public delegate void TowerClickHandler (TowerMaster towerMaster);
	public static event TowerClickHandler onTowerClick;

	public delegate void WaveSpawnerEventHandler ();
	public static event WaveSpawnerEventHandler onEnemySpawned;
	public static event WaveSpawnerEventHandler onAllEnemiesSpawned;

	public delegate void LevelEventHandler ();
	public static event LevelEventHandler onLevelWon;

	//this is called by whoever needs to raise the event
	public static void EnemyDestroyed(int value) {
		if (onEnemyDestroy != null) {
			onEnemyDestroy (value);
		} else {
			Debug.Log ("Event called without subscribers");
		}
	}

	//this is called by whoever needs to raise the event
	public static void EnemyReachedEnd(int value) {
		if (onEnemyReachedEnd != null) {
			onEnemyReachedEnd (value);
		} else {
			Debug.Log ("Event called without subscribers");
		}
	}

	public static void TowerBuilt(int value) {
		if (onTowerBuilt != null) {
			onTowerBuilt (value);
		} else {
			Debug.Log ("Event called without subscribers");
		}
	}

	public static void EmptyNodeClick(Node node) {
		if (onEmptyNodeClick != null) {
			onEmptyNodeClick (node);
		} else {
			Debug.Log ("Event called without subscribers");
		}
	}

	public static void TowerClick(TowerMaster towerMaster) {
		if (onTowerClick != null) {
			onTowerClick (towerMaster);
		} else {
			Debug.Log ("Event called without subscribers");
		}
	}

	public static void EnemySpawned() {
		if (onEnemySpawned != null) {
			onEnemySpawned ();
		} else {
			Debug.Log ("Event called without subscribers");
		}
	}

	public static void AllEnemiesSpawned() {
		if (onAllEnemiesSpawned != null) {
			onAllEnemiesSpawned ();
		} else {
			Debug.Log ("Event called without subscribers");
		}
	}

	public static void LevelWon() {
		if (onLevelWon != null) {
			onLevelWon ();
		} else {
			Debug.Log ("Event called without subscribers");
		}
	}

}
