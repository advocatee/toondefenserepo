﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TowerBlueprint {
	public string textName;
	public GameObject prefab;
	public int cost;
	public Sprite buttonThumbnail;

}

[System.Serializable]
public class TowerRoadmap {

	public TowerBlueprint[] towerBlueprints;

}

public class TowerDictionary : MonoBehaviour {

	public static TowerDictionary instance;



	public TowerRoadmap[] towerRoadmaps;



	//called before Start()
	void Awake() {
		if (instance != null) {
			Debug.Log ("More than one TowerDictionary in scene!");
			return;
		}	
		instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public TowerBlueprint[] GetBaseTowers() {
		return null;
	}

	public TowerBlueprint GetTowerBlueprint(GameObject prefabToFind) {
		
		foreach (TowerRoadmap towerRoadmap in towerRoadmaps) {

			foreach (TowerBlueprint towerBlueprint in towerRoadmap.towerBlueprints) {
				if (towerBlueprint.prefab == prefabToFind) {
					return towerBlueprint;
				}
			}
		

		}
		return null;
	}

	public TowerBlueprint GetTowerUpgradeBlueprint(GameObject prefabToFind) {

		foreach (TowerRoadmap towerRoadmap in towerRoadmaps) {

			foreach (TowerBlueprint towerBlueprint in towerRoadmap.towerBlueprints) {
				if (towerBlueprint.prefab == prefabToFind) {
					return towerBlueprint;
				}
			}


		}
		return null;
	}
}
