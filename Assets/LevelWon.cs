﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelWon : MonoBehaviour {

//	public Text roundsSurvivedText;

	public GameObject starContainer;
	public GameObject starPrefab;
	public GameObject doneButton;

	void OnEnable () {
		StartCoroutine (PlayAnimations ());
	}

	IEnumerator PlayAnimations() {
		//		Display star animation
		int rating = PlayerStats.GetRating ();
		Debug.Log ("Rating of " + rating + " stars");

		for (int i = 0; i < rating; i++) {
			Debug.Log ("Instantiating star number " + i);
			Instantiate (starPrefab, transform.position, transform.rotation, starContainer.transform).SetActive(true);
			yield return new WaitForSeconds (0.5f);

		}
		doneButton.SetActive (true);


	}

	public void Done() {
		SceneManager.LoadScene ("LevelSelect");
	}
}
