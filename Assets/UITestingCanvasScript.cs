﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UITestingCanvasScript : MonoBehaviour {


	//https://docs.unity3d.com/ScriptReference/EventSystems.EventSystem.IsPointerOverGameObject.html
	//Note that for touch, IsPointerOverGameObject should be used with OnMouseDown() or Input.GetMouseButtonDown(0) or Input.GetTouch(0).phase == TouchPhase.Began.


	public Text text;

	
	// Update is called once per frame
	void Update()
	{
		// Check if there is a touch
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
		{
			// Check if finger is over a UI element
			if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
			{
				PrintMessages ("UITestingCanvasScript: Update: Clicked");
			}
		}
	}


	public void ButtonClick() {
		PrintMessages ("Button: Clicked");
	}

	public void PrintMessages(string value) {
		Debug.Log (value);
		text.text = value;
	} 
}
