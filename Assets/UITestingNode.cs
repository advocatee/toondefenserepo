﻿
using UnityEngine;
using UnityEngine.UI;

//use EventSystems so that onMOuseEnter onMouseDown don't trigger if something is 
//between the mouse and the node (e.g some other UI element)
using UnityEngine.EventSystems; 


public class UITestingNode : MonoBehaviour {

	public GameObject canvas;
	public Color altColor;

//	private UITestingCanvasScript script;
	private Renderer rend;
	private Color startColor;

	void Start() {
//		script = canvas.GetComponent<UITestingCanvasScript> ();
		rend = GetComponent<Renderer> ();
		startColor = rend.material.color;
		Debug.Log (startColor);
	}



	void OnMouseUp() {

		if (EventSystem.current.IsPointerOverGameObject (-1)) {
			Debug.Log ("Node: IsPointerOverGameObject -1");
		} else if (EventSystem.current.IsPointerOverGameObject (0)) {
			Debug.Log ("Node: IsPointerOverGameObject 0");
		} else if (EventSystem.current.IsPointerOverGameObject (1)) {
			Debug.Log ("Node: IsPointerOverGameObject 1");
		} else {
			ToggleColor ();
			
		}




	



	}

	public void ToggleColor() {
		if (rend.material.color == altColor) {
			rend.material.color = startColor;
		} else {
			rend.material.color = altColor;
		}
	}


}
