﻿using System.Collections;

using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public int levelNumberForPlayerPrefs;

//	public static bool GameIsOver;

	enum LevelState {Playing, Paused, Lost, Won};
	private LevelState levelState;

	public GameObject gameOverUI;
	public GameObject levelWonUI;

	//	private bool allEnemiesSpawned;
	private int enemiesAlive = 0;
	public Text enemiesAliveText;
	private bool allEnemiesSpawned = false;

	public Text levelNumber;

	// Use this for initialization
	void Start () {
		int levelReached = PlayerPrefs.GetInt ("levelReached" ,1);
		Debug.Log ("PlayerPref level reached is currently " + levelReached);

		levelNumber.text = "Level " + levelNumberForPlayerPrefs;
//		GameIsOver = false;
		
		levelState = LevelState.Playing;
		EventManager.onLevelWon += EndGameWon;
		EventManager.onAllEnemiesSpawned += this.AllEnemiesSpawned;
		EventManager.onEnemySpawned += this.IncreaseEnemiesAlive;
		EventManager.onEnemyDestroy += this.ReduceEnemiesAlive;
		EventManager.onEnemyReachedEnd += this.ReduceEnemiesAlive;
	}

	void OnDestroy() {
		EventManager.onLevelWon -= EndGameWon;
		EventManager.onAllEnemiesSpawned -= this.AllEnemiesSpawned;
		EventManager.onEnemySpawned -= this.IncreaseEnemiesAlive;
		EventManager.onEnemyDestroy -= this.ReduceEnemiesAlive;
		EventManager.onEnemyReachedEnd -= this.ReduceEnemiesAlive;
	}

	void AllEnemiesSpawned() {
		allEnemiesSpawned = true;
	}

	void IncreaseEnemiesAlive() {
		enemiesAlive++;
		enemiesAliveText.text = enemiesAlive.ToString();
	}

	void ReduceEnemiesAlive(int val) {
		enemiesAlive--;
		enemiesAliveText.text = enemiesAlive.ToString();
		if (allEnemiesSpawned && enemiesAlive == 0 && PlayerStats.GetLives() > 0) {
			EndGameWon ();
		}
		if (allEnemiesSpawned && enemiesAlive == 0 && PlayerStats.GetLives() == 0) {
			EndGameLost ();
		}
	}

	// Update is called once per frame
	void Update () {

//		if (GameIsOver)
//			return;

		if (levelState == LevelState.Lost)
			return;


		if (Input.GetKeyDown("v")) {
			EndGameWon ();
		}
		if (Input.GetKeyDown("e")) {
			EndGameLost ();
		}

		if (PlayerStats.GetLives() <= 0) {
			EndGameLost ();
		}
	}

	private void EndGameLost() {
		//GameIsOver = true;
		levelState = LevelState.Lost;
		Debug.Log ("Level Lost");
		gameOverUI.SetActive (true);
	}

	private void EndGameWon() {
		int levelReached = PlayerPrefs.GetInt ("levelReached", 1);
		Debug.Log ("PlayerPref level reached is currently " + levelReached);

		int nextLevelReached = levelNumberForPlayerPrefs + 1;
		Debug.Log ("nextLevelreached  is calculated as " + nextLevelReached);
		if (nextLevelReached > levelReached) {
			PlayerPrefs.SetInt ("levelReached", nextLevelReached);
			Debug.Log ("Setting PlayerPref levelReached as " + nextLevelReached);

		}
		
		levelState = LevelState.Won;
		Debug.Log ("Level Won");
		levelWonUI.SetActive (true);
	}
}
